$(function () {
  // Functions ==============================

  /**
   * @author odin
   * @description 自動計算活動費用
   */
  function autoCalculateFee() {
    const numberOfPeople = parseInt($('#activity_entering_number').val());
    const returnStr = 'NT$ ' + numberOfPeople * activityFee;

    $('#activity_entering_fee').text(returnStr);
    $('#activit_total_fee').val(numberOfPeople * activityFee);
  }

  // Varables =================================================

  // 活動費用/人
  const activityFee = parseInt($('#activit_fee').val());

  // Doms
  const $form = $('#activity_form');
  const $activityEnteringNumber = $('#activity_entering_number');
  const $activitySubmitBtn = $('#activity_submit_btn');
  const $activityInput = $('.activity_input');

  // Initialization =================================================

  autoCalculateFee();

  // Events =================================================

  // 自動計算報名費用
  $activityEnteringNumber.bind('change', function () {
    autoCalculateFee();
  });

  // 輸入檢查
  $activityInput.bind(
    'keyup',
    debounce(function () {
      const addErrorClassDom = $(this).parent();
      const value = $(this).val().trim();
      const type = $(this).data('type');

      // error class toggle
      if (type === 'email') {
        if (!isEmpty(value) && isEmailCorrect(value)) {
          addErrorClassDom.removeClass('error');
        } else {
          addErrorClassDom.addClass('error');
        }
      } else {
        if (!isEmpty(value)) {
          addErrorClassDom.removeClass('error');
        } else {
          addErrorClassDom.addClass('error');
        }
      }
    }, 500),
  );

  // 送出表單
  $activitySubmitBtn.bind('click', function (e) {
    e.preventDefault();

    let allPass = true;
    const name = $('#activity_form_name').val().trim();
    const email = $('#activity_form_email').val().trim();
    const phoneNumber = $('#activity_form_phone').val().trim();

    // 姓名
    if (isEmpty(name)) {
      $('#activity_form_name').parent().addClass('error');
      allPass = false;
    }

    // 電話號碼檢查
    if (isEmpty(phoneNumber)) {
      $('#activity_form_phone').parent().addClass('error');
      allPass = false;
    }

    // Email 檢查
    if (isEmpty(email) || !isEmailCorrect(email)) {
      $('#activity_form_email').parent().addClass('error');
      allPass = false;
    }

    if (allPass) {
      // 送出表單
      $form.submit();
    }
  });
});
