$(function () {
  // Varables =================================================

  // Doms
  const $jsPublishedBtn = $('.js_published_btn');
  const $jsPpublishArticleWrapper = $('.js_publish_article_wrapper');

  // Events =================================================

  // 切換類別
  $jsPublishedBtn.bind('click', function () {
    const target = $(this).data('target');

    // 切換樣式
    $jsPublishedBtn.removeClass('active');
    $(this).addClass('active');

    // 顯示的文章容器切換
    $jsPpublishArticleWrapper.addClass('hidden');
    $(target).removeClass('hidden');
  });
});
