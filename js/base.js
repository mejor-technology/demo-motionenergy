/**
 * @author odin
 * @description 判斷是否該值為空
 * @param  {Primitive value} value 輸入的值
 * @returns {boolean or null}
 */
function isEmpty(value) {
  if (value === 0) {
    return false;
  } else {
    if (value && value !== '' && value !== null && value !== undefined) {
      return false;
    } else {
      return true;
    }
  }
}

/**
 * @author odin
 * @description 判斷email是否有符合規則
 * @param  {string} value - 要被檢查的字串
 * @returns {boolean}
 */
function isEmailCorrect(value) {
  // eslint-disable-next-line
  const rule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  if (value.search(rule) == -1) {
    return false;
  } else {
    return true;
  }
}

/**
 * @author odin
 * @param {function} func - callbackFunc
 * @param {number} delay - 要延遲幾秒執行
 * @description 代理模式 -- 等到最後一次function被觸發的時候，才開始計算timeout，等到timeout結束才抓取資料。
 */
function debounce(func, delay) {
  // timeout 初始值
  let timeout = null;
  return function () {
    let context = this; // 指向 myDebounce 這個 input
    let args = arguments; // KeyboardEvent
    clearTimeout(timeout);

    timeout = setTimeout(function () {
      func.apply(context, args);
    }, delay);
  };
}

/**
 * @author odin
 * @param {Object} input -- 指向該 input 的 Dom
 * @param {Function} func -- callback function
 * @param {Function} func2 -- 讀完圖片後執行的 callback function
 * @description 讀取上傳的圖片檔案並且顯示
 */
function readURL(input, func, func2) {
  if (input.files && input.files[0]) {
    reader = new FileReader();

    reader.onload = function (e) {
      func(e);
    };

    reader.readAsDataURL(input.files[0]);

    func2 instanceof Function ? func2() : console.log('Without Func2');
  }
}

/**
 * @author odin
 * @description 調整超過 1920 之後 漢堡選單展開的樣子
 */
function adjustNavRight() {
  let width = $(window).width();

  if (width >= 1400) {
    let leftDistance = $('.dropdown-menu').offset().left;
    let intercept = width - leftDistance - 36;

    console.log('intercept', intercept);

    $('#dropdown-list').css('right', intercept);
  } else {
    $('#dropdown-list').css('right', '');
  }
}

/**
 * @author odin
 * @description 裝置寬度超過 1400px 調整橘色容器左邊的padding數值
 */
function adjustPrimaryLinksContainerPaddingLeft() {
  let width = $(window).width();

  if (width >= 1400) {
    let paddingLeft = $('#logo').position().left;

    $('#member-primary-container').css('paddingLeft', paddingLeft);
  } else {
    $('#member-primary-container').removeAttr('style');
  }
}

/**
 * @author odin
 * @description 如果視窗的高度少於 member 左邊列表的高度，則調整 member-container 的 class
 */
function adjustMemberContainerHeight() {
  const $memberContainer = $('.member-container');
  const $memberSideColumn = $('#member-primary-container');
  const memberSideColumnHeight =
    $memberSideColumn.length > 0 ? $memberSideColumn.outerHeight() : 0;
  let width = $(window).width();
  let height = $(window).height();

  if (memberSideColumnHeight !== 0) {
    if (width > 768 && memberSideColumnHeight + 30 >= height) {
      $memberContainer.css('min-height', memberSideColumnHeight + 30 + 'px');
    } else {
      $memberContainer.css('min-height', '100vh');
    }
  } else {
    return;
  }
}

// 全域變數
const $mainRoot = $('.main-root');

$(function () {
  // resize 調整
  (function () {
    // Initialization
    adjustNavRight();

    // 超過 1400px 的寬度的時候調整 padding
    adjustPrimaryLinksContainerPaddingLeft();

    // 如果視窗的高度少於 member 左邊列表的高度，則調整 member-container 的 class
    adjustMemberContainerHeight();

    // resize
    $(window).resize(function () {
      adjustNavRight();
      adjustPrimaryLinksContainerPaddingLeft();
      adjustMemberContainerHeight();
    });
  })();

  // Member 相關的 js 控制
  (function () {
    const $mobileCategoryBtns = $('.m_member_category_btn');
    const $mobileSubCategoryBtnsContainers = $('.member-sub-category-group');
    const $mobileSubCategoryBtns = $('.m_member_sub_category_btn');

    $mobileCategoryBtns.bind('click', function () {
      const subCategoryContainerTarget = $(this).data('sub-container-target');
      const subCategoryDefaultTarget = $(this).data(
        'sub-category-default-target',
      );

      console.log('subCategoryContainerTarget', subCategoryContainerTarget);
      console.log('subCategoryDefaultTarget', subCategoryDefaultTarget);

      // 主分類按鈕 樣式處理
      $mobileCategoryBtns.removeClass('active');
      $(this).addClass('active');

      // 次分類 container 顯示
      $mobileSubCategoryBtnsContainers.removeClass('active');
      $(subCategoryContainerTarget).addClass('active');

      // 次分類按鈕 樣式處理
      $mobileSubCategoryBtns.removeClass('active');
      $(subCategoryDefaultTarget).addClass('active');

      // 不是本頁的話就直接點擊該連結換頁
      if (subCategoryContainerTarget !== '#member_accoount_sub_category') {
        const href = $(subCategoryDefaultTarget).attr('href');

        if (href !== 'javascript:void(0)') {
          window.location.href = href;
        }
      }
    });

    // $mobileCategoryBtns.bind('click', function () {
    //   const subCategoryContainerTarget = $(this).data('sub-container-target');
    //   const subCategoryDefaultTarget = $(this).data(
    //     'sub-category-default-target',
    //   );

    //   console.log('subCategoryContainerTarget', subCategoryContainerTarget);
    //   console.log('subCategoryDefaultTarget', subCategoryDefaultTarget);

    //   // 主分類按鈕 樣式處理
    //   $mobileCategoryBtns.removeClass('active');
    //   $(this).addClass('active');

    //   // 次分類 container 顯示
    //   $mobileSubCategoryBtnsContainers.removeClass('active');
    //   $(subCategoryContainerTarget).addClass('active');

    //   // 次分類按鈕 樣式處理
    //   $mobileSubCategoryBtns.removeClass('active');
    //   $(subCategoryDefaultTarget).addClass('active');
    // });
  })();

  // Tooltips 控制
  (function () {
    $mainRoot.on('click', '.tooltip_btn', function (e) {
      e.preventDefault();

      const isOpen = $(this).hasClass('open');
      const tooltipTarget = $(this).data('tooltip');

      if (isOpen) {
        // 關閉
        $(this).removeClass('open');
        $(tooltipTarget).addClass('hidden');
      } else {
        // 開啟
        $(this).addClass('open');
        $(tooltipTarget).removeClass('hidden');
      }
    });
  })();
});
