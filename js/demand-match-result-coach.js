$(function () {
  // Functions ==================================================
  /**
   * @author odin
   * @param {number} ms -- 間隔的毫秒數
   * @description 避免一直點
   * @return {boolean}
   */
  function stopClicking(ms) {
    clicking = false;
    setTimeout(function () {
      clicking = true;
    }, ms);
  }

  // Variables =================================================
  let clicking = true; // 避免一直被點
  // Dom
  const $toggleBtn = $('.toggle_body_btn');

  // Initialize ==============================

  // 如果要開啟| 關閉特定的燈箱，就使用 showLightbox($DomId) | closeLightbox($DomId)
  // showLightbox('#lightbox-show-contact-detail');

  // Events =================================================

  // toggle 內容
  $toggleBtn.bind('click', function (e) {
    e.preventDefault();

    if (clicking) {
      const $targetBody = $(this).siblings('.coach_match_result_body');
      const $unitDom = $(this).closest('.coach_match_result_unit');
      const isOpen = $unitDom.hasClass('open');

      if (isOpen) {
        $targetBody.slideUp();

        setTimeout(function () {
          $unitDom.removeClass('open');
        }, 500);
      } else {
        $unitDom.addClass('open');
        $targetBody.slideDown();

        // setTimeout(function () {
        //   $unitDom.removeClass('open');
        // }, 500);
      }

      // 避免一直點
      stopClicking(600);
    }
  });
});
