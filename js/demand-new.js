$(function () {
  // Functions ==================================================
  /**
   * @author odin
   * @description 分幾個部分進行檢查，全部通過就回傳 true 
   * @return {boolean}
   */
  function checkAllRight() {
    const isCheckboxPass = checkCheckboxes();
    const isCheckContact = checkContactApproaching();

    return isCheckboxPass && isCheckContact;
  }

  /**
   * @author odin
   * @description 檢查所有的checkbox組是否都有勾選
   * @return {boolean}
   */
  function checkCheckboxes() {
    let isPass = true;
    const checkList = [
      '.checkbox_item',
      '.checkbox_sub_item',
      '.checkbox_age',
      '.checkbox_gender',
      '.checkbox_mode',
      '.checkbox_time',
      '.checkbox_week',
      '.checkbox_time_intercept',
    ];

    checkList.forEach(function (item) {
      const totalLength = $(item).length;
      let count = 0;
      $(item).each(function (index, checkboxDom) {
        $(this).prop('checked') ? count++ : null;

        // 最後一個得時候判斷是否要加 error class
        if (totalLength - 1 === index) {
          if (count === 0) {
            $(this).closest('.service_form_unit').addClass('error');
            isPass = false;
            return;
          } else if (count > 0) {
            $(this).closest('.service_form_unit').removeClass('error');
          }
        }
      });
    });

    console.log('isPass', isPass);

    return isPass;
  }

  /**
   * @author odin
   * @description 檢查所有的checkbox組是否都有勾選
   * @return {boolean}
   */
  function checkContactApproaching() {
    let isPass = true;
    let checkCount = 0; // 勾了幾個
    let formCount = 0; // 有符合形式的有幾個
    const totalLength = $('.checkbox_contact').length; // 總共有幾個

    $('.checkbox_contact').each(function (index, dom) {
      const isChecked = $(this).prop('checked');
      const val = $(this).siblings('.contact_input').val().trim();

      console.log('isChecked', isChecked, index);
      console.log('val', val, index);

      if (isChecked) {
        checkCount++;

        if (!isEmpty(val)) {
          formCount++;
        }
      } else {
        if (isEmpty(val)) {
          formCount++;
        }
      }
    });

    // 確認是不是都沒有勾
    if (checkCount === 0) {
      $('.contact_email').closest('.service_form_unit').addClass('error');

      isPass = false;
    } else {
      // 確認是不是勾的形式上市正確的(有勾的有填，沒勾的沒填)
      if (totalLength === formCount) {
        $('.contact_email').closest('.service_form_unit').removeClass('error');
      } else {
        $('.contact_email').closest('.service_form_unit').addClass('error');

        isPass = false;
      }
    }

    return isPass;
  }

  // Events =================================================

  // 點下 儲存 後
  $('#add_new_demand_btn').bind('click', function (e) {
    e.preventDefault();
    const allPass = checkAllRight();

    if (allPass) {
      // 成功的 API
    }
  });
});
