$(function () {
  let isOpen = false;

  // Doms
  const $HamburgerBtn = $('#hamburger_btn');
  const $DropdownMenu = $('#dropdown-menu');
  const $DropdownList = $('#dropdown-list');

  // Events
  if ($HamburgerBtn.length > 0) {
    $HamburgerBtn.bind('click', function () {
      isOpen = !isOpen;

      if (isOpen) {
        $DropdownMenu.addClass('opend');
        $DropdownList.slideDown();
      } else {
        $DropdownMenu.removeClass('opend');
        $DropdownList.slideUp();
      }
    });
  }
});
