$(function () {
  // Variables ===========================

  // Dom Element
  const $slickContainer = $('#carousel_container');
  const $teacherContainer = $('#teacher_carousel');

  // Initialization ===========================
  // Carousel Init
  $slickContainer.slick({
    autoplay: true,
    arrows: false,
    dots: true,
  });

  $teacherContainer.slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      // {
      //   breakpoint: 768,
      //   settings: {
      //     slidesToShow: 2,
      //     slidesToScroll: 2,
      //   },
      // },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
});
