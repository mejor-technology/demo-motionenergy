// $(function () {
// Functions ===========================

function showLightbox($DomId) {
  lightboxContainer.show();
  lightboxMask.show();
  $($DomId).show();
  $('body').addClass('overflow-y-hidden');
}

function closeLightbox($DomId) {
  lightboxContainer.hide();
  lightboxMask.hide();
  $($DomId).hide();
  $('body').removeClass('overflow-y-hidden');
}

// Variables ===========================

// Doms
const lightboxContainer = $('#lightbox-container');
const lightboxMask = $('#lightbox-mask');
const closeLightboxBtn = $('.close_lightbox');

// Initialization ===========================

// showLightbox('#lightbox-alert');

// Events ===========================

// 關閉燈箱
closeLightboxBtn.bind('click', function () {
  const closeLightboxDomIdSelector = $(this).data('close-lightbox-dom');

  if (closeLightboxDomIdSelector) {
    closeLightbox(closeLightboxDomIdSelector);
  }
});
// });
