$(function () {
  // functions ==============================

  function init() {
    const initTargetSelector = $('.switch_container_btn.active').data('target');

    $(initTargetSelector).removeClass('hidden');
  }

  // Varables ==============================

  // Doms
  const $switchContainerBtn = $('.switch_container_btn');
  const $mobContainers = $('.mob_entering_data_box');
  const $webContainers = $('.entering_data_form_box');

  // Initialization ===========================
  init();

  // Events ===========================
  $switchContainerBtn.bind('click', function () {
    const targetSelector = $(this).data('target');

    // 按鈕樣式交換
    $switchContainerBtn.removeClass('active');
    $(this).addClass('active');

    // 顯示內容控制
    $mobContainers.addClass('hidden');
    $webContainers.addClass('hidden');

    $(targetSelector).removeClass('hidden');
  });
});
