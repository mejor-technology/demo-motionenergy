$(function () {
  // Functions =================================================================
  /**
   * @author odin
   * @param {array} checkArr -- 包含每一個欄位的Dom以及value的陣列
   * @param {string} type -- corporation | personal
   * @description 檢查並且顯示是否有錯誤
   * @return {boolean} -- 全部都無誤就回傳 true
   */
  function checkMemberInputData(checkArr, type) {
    let count = 0;
    // 加 1 是多圖片的檢查
    let total = checkArr.length + 1;
    let allPass = null;

    // 檢查欄位
    checkArr.forEach(function (item) {
      const type = $(item.dom).attr('type');

      if (type === 'email') {
        if (!isEmpty(item.value) && isEmailCorrect(item.value)) {
          $(item.dom).closest('.member-group').removeClass('error');

          count++;
        } else {
          $(item.dom).closest('.member-group').addClass('error');
        }
      } else {
        if (!isEmpty(item.value)) {
          $(item.dom).closest('.member-group').removeClass('error');
          count++;
        } else {
          $(item.dom).closest('.member-group').addClass('error');
        }
      }
    });

    // 檢查圖片
    if (type === 'corporation') {
      if (corporationUploadedArray.length > 0) {
        count++;

        $('#corporation_certificate_upload_section').removeClass('error');
      } else {
        $('#corporation_certificate_upload_section').addClass('error');
      }
    } else if (type === 'personal') {
      if (personalUploadedArray.length > 0) {
        count++;

        $('#personal_certificate_upload_section').removeClass('error');
      } else {
        $('#personal_certificate_upload_section').addClass('error');
      }
    }

    allPass = count === total ? true : false;

    return allPass;
  }

  /**
   * @author odin
   * @param {string} type -- 要產生哪種類型的上傳 group
   * @description 產生一個上傳的 group 到特定的位置
   */
  function generateUploadGroup(type) {
    let $destinationDom = null;
    let targetArr = null;

    if (type === 'corporation') {
      $destinationDom = $('#upload_corporation_container');

      // 每次新增增加數量
      corporationUploadArray.push(corporationUploadArray.length + 1);

      targetArr = corporationUploadArray;
    } else {
      $destinationDom = $('#upload_personal_container');

      // 每次新增增加數量
      personalUploadArray.push(personalUploadArray.length + 1);

      targetArr = personalUploadArray;
    }

    // 作為模板的編號
    const order = targetArr.length;

    const template = `
    <div class="upload_certificate_group">

      <input type="file"
        class="hidden member-upload member_upload_certificate_input ${type}_certificate${order}"
        data-type="${type}"
        name="${type}_certificate${order}" id="${type}_certificate${order}">

      <label class="upload_certificate_label upload_certificate_label${order}" data-type="${type}">點選請上傳專業證照<br />尺寸大小400x300<br />照片總大小不超過1M</label>

      <img src="" class="hidden member-uploaded-img uploaded_${type}_certificate_img${order}" alt="Uploaded Image">

      <!-- 刪除的按鈕 -->
      <div class="hidden delete_btn_box">
        <button class="btn btn-type-img delete_img_btn delete_img_btn1-1">
          <img src="./img/icon_trashcan@2x.png" class="delete_img" alt="Delete Image">
        </button>
      </div>
      
    </div>`;

    $destinationDom.append(template);
  }

  // Variables ===============================
  // data
  // 產生新的上傳 group 的依據
  let corporationUploadArray = [];
  let personalUploadArray = [];

  // 已經上傳圖片的物件
  let corporationUploadedArray = [];
  let personalUploadedArray = [];

  // Doms
  const $switchMemberFileBtn = $('.switch_member_file_btn');
  const $jsMemberProFileShowSection = $('.js_member_pro_file_show_section');
  const $saveCorporationData = $('#save_corporation_data');
  const $savePersonalData = $('#save_personal_data');
  const $addUploadGroupBtn = $('.add_upload_group_btn');
  const $uploadContainer = $('.upload_container');
  const $unUploadBox = $('#unupload_box');
  const $profileImgBox = $('#profile_img_box');

  // Initialization ===============================

  generateUploadGroup('corporation');
  generateUploadGroup('personal');

  // 開啟燈箱
  // showLightbox('#lightbox-cropper');

  // Events ===============================

  // 切換公司 | 個人資料
  $switchMemberFileBtn.bind('click', function () {
    const target = $(this).data('target');

    // 切換樣式
    $switchMemberFileBtn.removeClass('active');
    $(this).addClass('active');

    // 切換顯示
    $jsMemberProFileShowSection.addClass('hidden');
    $(target).removeClass('hidden');
  });

  // 點下 公司資訊 的儲存
  $saveCorporationData.bind('click', function (e) {
    e.preventDefault();

    const corporation_name = $('#corporation_name').val().trim();
    const corporation_owner = $('#corporation_owner').val().trim();
    const corporation_size = $('#corporation_size').val().trim();
    const corporation_url = $('#corporation_url').val().trim();
    const corporation_introduction = $('#corporation_introduction')
      .val()
      .trim();
    const corporation_phone = $('#corporation_phone').val().trim();
    const corporation_email = $('#corporation_email').val().trim();
    const corporation_lineid = $('#corporation_lineid').val().trim();

    const checkArr = [
      {
        dom: '#corporation_name',
        value: corporation_name,
      },
      {
        dom: '#corporation_owner',
        value: corporation_owner,
      },
      {
        dom: '#corporation_size',
        value: corporation_size,
      },
      {
        dom: '#corporation_url',
        value: corporation_url,
      },
      {
        dom: '#corporation_introduction',
        value: corporation_introduction,
      },
      {
        dom: '#corporation_phone',
        value: corporation_phone,
      },
      {
        dom: '#corporation_email',
        value: corporation_email,
      },
      {
        dom: '#corporation_lineid',
        value: corporation_lineid,
      },
    ];

    const checkResult = checkMemberInputData(checkArr, 'corporation');

    if (checkResult) {
      // 全部的欄位都通過檢查
    }
  });

  // 點下 個人資訊 的儲存
  $savePersonalData.bind('click', function (e) {
    e.preventDefault();

    const personal_name = $('#personal_name').val().trim();
    const personal_url = $('#personal_url').val().trim();
    const personal_introduction = $('#personal_introduction').val().trim();
    const personal_phone = $('#personal_phone').val().trim();
    const personal_email = $('#personal_email').val().trim();
    const personal_lineid = $('#personal_lineid').val().trim();

    const checkArr = [
      {
        dom: '#personal_name',
        value: personal_name,
      },
      {
        dom: '#personal_url',
        value: personal_url,
      },
      {
        dom: '#personal_introduction',
        value: personal_introduction,
      },
      {
        dom: '#personal_phone',
        value: personal_phone,
      },
      {
        dom: '#personal_email',
        value: personal_email,
      },
      {
        dom: '#personal_lineid',
        value: personal_lineid,
      },
    ];

    const checkResult = checkMemberInputData(checkArr, 'personal');

    if (checkResult) {
      // 全部的欄位都通過檢查
    }
  });

  // 輸入檢查一律寫在 member.js

  // 增加一組上傳的 group 的按鈕被點下後
  $addUploadGroupBtn.bind('click', function (e) {
    e.preventDefault();

    const type = $(this).data('type');

    generateUploadGroup(type);
  });

  // 上傳照片
  $uploadContainer.on('click', '.upload_certificate_label', function () {
    $(this).siblings('.member-upload').trigger('click');
  });

  $uploadContainer.on('change', '.member-upload', function () {
    const vm = this;
    const $targetImgDom = $(this).siblings('.member-uploaded-img');
    const $siblingsLabelDom = $(this).siblings('.upload_certificate_label');
    const type = $(this).data('type');
    const $delete = $(this).siblings('.delete_btn_box');
    const $container = $(this).closest('.upload_certificate_group');

    // 要給 function 指定特定的 Dom 元素操作
    readURL(vm, function (e) {
      console.log('readURL e', e);
      if (e.target.result) {
        $targetImgDom.attr('src', e.target.result).removeClass('hidden');
        $siblingsLabelDom.addClass('hidden');
        $siblingsLabelDom.addClass('hidden');
        $delete.removeClass('hidden');
        $container.addClass('upload');

        if (type === 'corporation') {
          // 放入已經上傳的照片物件
          corporationUploadedArray.push(e);

          // 移除錯誤提示
          $('#corporation_certificate_upload_section').removeClass('error');
        } else if (type === 'personal') {
          // 放入已經上傳的照片物件
          personalUploadedArray.push(e);

          // 移除錯誤提示
          $('#personal_certificate_upload_section').removeClass('error');
        }
      }
    });
  });

  // 更換頭像
  (function () {
    let cropper = null;
    const $cropImg = $('#cropper_img');

    // 觸發input
    $('#change_profile_img_btn').bind('click', function () {
      $(this)
        .closest('.profile_img_box')
        .find('#profile_img_input')
        .trigger('click');
    });

    // 上傳照片之後
    $('#profile_img_input').bind('change', function () {
      const vm = this;

      // 要給 function 指定特定的 Dom 元素操作
      readURL(vm, function (e) {
        console.log('readURL e', e);
        if (e.target.result) {
          let imgSrc = e.target.result;
          $cropImg.attr('src', imgSrc);
          $cropImg.cropper('replace', imgSrc, false);
        }
      });

      // 開啟燈箱
      showLightbox('#lightbox-cropper');
    });

    // cropper圖片裁剪
    $cropImg.cropper({
      aspectRatio: 1 / 1, // 預設比例
      // preview: '#previewImg', // 預覽檢視
      guides: false, // 裁剪框的虛線(九宮格)
      autoCropArea: 1, // 0-1之間的數值，定義自動剪裁區域的大小，預設0.8
      dragMode: 'crop', // 拖曳模式 crop(Default,新增裁剪框) / move(移動裁剪框&圖片) / none(無動作)
      cropBoxResizable: true, // 是否有裁剪框調整四邊八點
      movable: true, // 是否允許移動圖片
      zoomable: true, // 是否允許縮放圖片大小
      rotatable: false, // 是否允許旋轉圖片
      zoomOnWheel: true, // 是否允許通過滑鼠滾輪來縮放圖片
      zoomOnTouch: true, // 是否允許通過觸控移動來縮放圖片
      ready: function (e) {
        console.log('ready!');
      },
    });

    // 按下裁切之後
    $('#lightbox_cropper_btn').bind('click', function () {
      const showImgSelector = $(this).data('img-dom');
      const inputSelector = $(this).data('input-dom');

      if (!$cropImg.attr('src')) {
        return false;
      } else {
        let cropImg = $cropImg.cropper('getData');
        let cvs = $cropImg.cropper('getCroppedCanvas'); // 獲取被裁剪後的canvas
        let context = cvs.getContext('2d');
        let base64 = cvs.toDataURL('image/jpeg'); // 轉換為base64

        let img = new Image();

        console.log('cropImg', cropImg);
        console.log('cvs', cvs);
        console.log('context', context);
        console.log('base64', base64);

        img.src = base64;

        // 把裁切的 base64 放到對應的位置
        $(showImgSelector).attr('src', base64);
        $(inputSelector).val(base64);
        $(showImgSelector).removeClass('hidden');
        $unUploadBox.addClass('hidden');
        $profileImgBox.addClass('uploaded');
      }
    });
  })();
});
