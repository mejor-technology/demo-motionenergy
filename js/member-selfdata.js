$(function () {
  // Functions ===============================

  /**
   * @author odin
   * @param {boolean} isEnable -- 是否可以編輯
   * @description 是否可以編輯 input
   */
  function enableEdit(isEnable) {
    if (isEnable) {
      $('.member-input').removeAttr('readonly');
      $('.member-textarea').removeAttr('readonly');
    } else {
      $('.member-input').addeAttr('readonly');
      $('.member-textarea').addeAttr('readonly');
    }
  }

  /**
   * @author odin
   * @description 確認個人編輯資料是否符合規定
   * @return {boolean} 是否都通過檢查
   */
  function checkSelfData() {
    let allPass = true;
    const memberName = $('#member_name').val();
    const memberEmail = $('#member_email').val();
    const memberPhone = $('#member_phone').val();
    const memberIntroduction = $('#member_introduction').val();

    // 姓名檢查
    if (isEmpty(memberName)) {
      $('#member_name').parent().addClass('error');
      allPass = false;
    }

    // Email 檢查
    if (isEmpty(memberEmail) || !isEmailCorrect(memberEmail)) {
      $('#member_email').parent().addClass('error');
      allPass = false;
    }

    // 電話檢查
    if (isEmpty(memberPhone)) {
      $('#member_phone').parent().addClass('error');
      allPass = false;
    }

    // 自我介紹檢查
    if (isEmpty(memberIntroduction)) {
      $('#member_introduction').parent().addClass('error');
      allPass = false;
    }

    return allPass;
  }

  /**
   * @author odin
   * @param {boolean} isEnable -- 是否進入修改密碼的模式
   * @description 是否進入修改密碼的模式
   */
  function enableEditPwd(isEnable) {
    if (isEnable) {
      $memberModifyPwdSection.removeClass('hidden');
      $memberSelfdataSection.addClass('hidden');
    } else {
      $memberModifyPwdSection.addClass('hidden');
      $memberSelfdataSection.removeClass('hidden');
    }
  }

  function checkChangePwd() {
    let allPass = true;
    const memberPwd = $('#member_pwd').val();
    const memberPwdConfirm = $('#member_pwd_confirm').val();

    // 密碼檢查
    if (isEmpty(memberPwd)) {
      $('#member_pwd').parent().addClass('error');
      allPass = false;
    }

    // 密碼一致性檢查
    if (isEmpty(memberPwdConfirm) || memberPwd !== memberPwdConfirm) {
      $('#member_pwd_confirm').parent().addClass('error');
      allPass = false;
    }
  }

  // Variables ===============================

  // Doms
  const $editSelfdataBtn = $('#edit_selfdata_btn');
  const $memberSelfdataSection = $('#member_selfdata_section');
  const $memberModifyPwdSection = $('#member_modify_pwd');
  const $saveSelfdataChangeBtn = $('#save_selfdata_change_btn');
  // const $memberInput = $('.member-input');
  const $memberTextArea = $('.member-textarea');
  const $modifyPwdBtn = $('#modify_pwd_btn');
  const $confirmPwdChangeBtn = $('#confirm_pwd_change_btn');
  const $uploadProfileImgBtn = $('#change_profile_img_btn');
  const $uploadImageInput = $('#upload_profile_img_input');
  const $uploadCertificateGroup = $('.upload_certificate_group');
  const $unUploadBox = $('#unupload_box');

  // Initialization ===============================

  // Events ===============================

  // resize
  $(window).resize(function () {
    adjustPrimaryLinksContainerPaddingLeft();
  });

  // 進入編輯模式
  $editSelfdataBtn.bind('click', function () {
    $memberSelfdataSection.addClass('editing');

    // 可以編輯
    enableEdit(true);
  });

  // 送出 個人資料編輯 資料
  $saveSelfdataChangeBtn.bind('click', function () {
    // 檢查填入的 data 是否正確
    const isAllPass = checkSelfData();

    if (isAllPass) {
      // 做送出的動作，並且更新畫面的資料

      // 關閉編輯狀態
      enableEdit(false);
    }
  });

  // 輸入檢查
  // $memberInput.bind(
  //   'keyup',
  //   debounce(function () {
  //     const value = $(this).val();
  //     const type = $(this).attr('type');

  //     // error class toggle
  //     if (type === 'email') {
  //       if (!isEmpty(value) && isEmailCorrect(value)) {
  //         $(this).closest('.member-group').removeClass('error');
  //       } else {
  //         $(this).closest('.member-group').addClass('error');
  //       }
  //     } else {
  //       if (value.trim() !== '') {
  //         $(this).closest('.member-group').removeClass('error');
  //       } else {
  //         $(this).closest('.member-group').addClass('error');
  //       }
  //     }
  //   }, 500),
  // );

  $memberTextArea.bind(
    'keyup',
    debounce(function () {
      const value = $(this).val();

      // error class toggle
      if (value.trim() !== '') {
        $(this).closest('.member-group').removeClass('error');
      } else {
        $(this).closest('.member-group').addClass('error');
      }
    }, 500),
  );

  // 進入修改密碼的模式
  $modifyPwdBtn.bind('click', function () {
    enableEditPwd(true);
  });

  // 送出要修改的密碼
  $confirmPwdChangeBtn.bind('click', function () {
    const isAllPass = checkChangePwd();

    if (isAllPass) {
      // 做送出的動作，並且更新畫面的資料

      // 關閉編輯狀態
      enableEditPwd(false);
    }
  });

  (function () {
    let cropper = null;
    const $cropImg = $('#cropper_img');

    // 觸發input
    $('#change_profile_img_btn').bind('click', function () {
      $(this)
        .closest('.member_profile_img_box')
        .find('#upload_profile_img_input')
        .trigger('click');
    });

    // 上傳照片之後
    $('#upload_profile_img_input').bind('change', function () {
      const vm = this;

      // 要給 function 指定特定的 Dom 元素操作
      readURL(vm, function (e) {
        console.log('readURL e', e);
        if (e.target.result) {
          let imgSrc = e.target.result;
          $cropImg.attr('src', imgSrc);
          $cropImg.cropper('replace', imgSrc, false);
        }
      });

      // 開啟燈箱
      showLightbox('#lightbox-cropper');
    });

    // cropper圖片裁剪
    $cropImg.cropper({
      aspectRatio: 1 / 1, // 預設比例
      // preview: '#previewImg', // 預覽檢視
      guides: false, // 裁剪框的虛線(九宮格)
      autoCropArea: 1, // 0-1之間的數值，定義自動剪裁區域的大小，預設0.8
      dragMode: 'crop', // 拖曳模式 crop(Default,新增裁剪框) / move(移動裁剪框&圖片) / none(無動作)
      cropBoxResizable: true, // 是否有裁剪框調整四邊八點
      movable: true, // 是否允許移動圖片
      zoomable: true, // 是否允許縮放圖片大小
      rotatable: false, // 是否允許旋轉圖片
      zoomOnWheel: true, // 是否允許通過滑鼠滾輪來縮放圖片
      zoomOnTouch: true, // 是否允許通過觸控移動來縮放圖片
      ready: function (e) {
        console.log('ready!');
      },
    });

    // 按下裁切之後
    $('#lightbox_cropper_btn').bind('click', function () {
      const showImgSelector = $(this).data('img-dom');
      const inputSelector = $(this).data('input-dom');

      if (!$cropImg.attr('src')) {
        return false;
      } else {
        let cropImg = $cropImg.cropper('getData');
        let cvs = $cropImg.cropper('getCroppedCanvas'); // 獲取被裁剪後的canvas
        let context = cvs.getContext('2d');
        let base64 = cvs.toDataURL('image/jpeg'); // 轉換為base64

        let img = new Image();

        console.log('cropImg', cropImg);
        console.log('cvs', cvs);
        console.log('context', context);
        console.log('base64', base64);

        img.src = base64;

        // 把裁切的 base64 放到對應的位置
        $(showImgSelector).attr('src', base64);
        $(inputSelector).val(base64);
        $(showImgSelector).removeClass('hidden');
        $unUploadBox.addClass('hidden');
        $memberSelfdataSection.addClass('uploaded');
      }
    });
  })();
  // // 上傳背景照片
  // $uploadProfileImgBtn.bind('click', function () {
  //   $uploadImageInput.trigger('click');
  // });

  // // 圖片上傳之後
  // $uploadImageInput.bind('change', function () {
  //   // 要給 function 指定特定的 Dom 元素操作
  //   readURL(this, function (e) {
  //     $('#member_profile_img').attr('src', e.target.result);
  //   });
  // });
});
