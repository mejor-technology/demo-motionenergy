$(function () {
  // Functions ==================================================

  /**
   * @author odin
   * @description 本頁的初始化
   */
  // function init() {
  //   // 產生第一個 服務相關照片 的html
  //   generateServiceRelatedUnit();

  //   // 產生第一個 服務案例 的html
  //   generateServiceCaseUnit();

  //   // 產生第一個 服務問答 的html
  //   generateQAUnit();
  // }

  /**
   * @author odin
   * @description 產生一個 服務相關照片 的 unit 到特定的位置
   */
  function generateServiceRelatedUnit() {
    const order =
      serviceRelatedUnitArray.length === 0
        ? 1
        : serviceRelatedUnitArray.length + 1;

    const nowTemplateObj = {
      order: order,
      imgObj: null,
    };

    // 初始化陣列內容
    serviceRelatedUnitArray.push(nowTemplateObj);

    const html = `
      <!-- 上傳一張證照的單位 -->
        <div class="upload_certificate_group" data-order="${order}">
          <!-- 實際上上傳的 input -->
          <input type="file"
            class="hidden member-upload member_upload_certificate_input service_related_upload service_related_upload${order}"
            name="service_related_upload${order}" id="service_related_upload${order}">

            <!-- 還沒上傳的時顯示的樣式 -->
          <label class="upload_certificate_label service_related_upload_label service_related_upload_label${order}" data-type="service_related">點選請上傳專業證照<br />尺寸大小400x300<br />照片總大小不超過1M</label>

          <!-- 上傳後的圖片會放在這裡 -->
          <img src="" class="hidden member-uploaded-img service_related_upload_img${order}"
            alt="Uploaded Image">
        </div>
                `;

    // 放置的Dom
    $uploadServiceRelatedContainer.append(html);
  }

  /**
   * @author odin
   * @description 產生一個 服務案例 的 unit 到特定的位置
   */
  function generateServiceCaseUnit() {
    const order =
      serviceCaseUnitArray.length === 0 ? 1 : serviceCaseUnitArray.length + 1;

    const nowTemplateObj = {
      order: order,
      title: '',
      description: '',
      imgObj: null,
    };

    // 初始化陣列內容
    serviceCaseUnitArray.push(nowTemplateObj);

    // 初始化模板
    const html = `
    ${
      // 如果不是第一個的話就要增加分隔線
      order !== 1 ? `<div class="seperator service_unit_seperator"></div>` : ''
    }
      <div class="service_case_unit" data-order="${order}">
        <!-- 服務案例: 標題 -->
        <div class="member-group">
          <input type="text" name="service_case_title${order}" id="service_case_title${order}"
            class="member-input service_case_title service_case_title${order}" placeholder="請輸入服務案例標題
            ">

          <span class="error_span member_error_hint">此項目不得為空</span>
        </div>

        <!-- 服務案例: 敘述 -->
        <div class="member-group">
          <input type="text" name="service_case_description${order}" id="service_case_description${order}"
            class="member-input service_case_description service_case_description${order}" placeholder="請輸入服務案例敘述
            ">

          <span class="error_span member_error_hint">此項目不得為空</span>
        </div>

        <!-- 服務案例: 照片上傳(不一定要上傳) -->
        <div class="upload_certificate_group upload_service_case_img_group">
          <!-- 實際上上傳的 input -->
          <input type="file"
            class="hidden member-upload member_upload_certificate_input service_case_img_upload service_case_img_upload${order}"
            name="service_case_img_upload${order}" id="service_case_img_upload${order}">

          <!-- 還沒上傳的時顯示的樣式 -->
          <label class="upload_certificate_label service_case_upload_label upload_certificate_label${order}"
            >點選請上傳服務案例照片</label>

          <!-- 上傳後的圖片會放在這裡 -->
          <img src="" class="hidden member-uploaded-img uploaded_service_case_img${order}" alt="Uploaded Image">
        </div>

      </div>
    `;

    // 放入 Dom 裡
    $('#service_case_body').append(html);
  }

  /**
   * @author odin
   * @description 產生一個 服務問答 的 unit 到特定的位置
   */
  function generateQAUnit() {
    const order =
      serviceQAUnitArray.length === 0 ? 1 : serviceQAUnitArray.length + 1;

    const nowTemplateObj = {
      order: order,
      question: '',
      answer: '',
    };

    // 初始化陣列內容
    serviceQAUnitArray.push(nowTemplateObj);

    const html = `
      ${
        // 如果不是第一個的話就要增加分隔線
        order !== 1
          ? `<div class="seperator service_unit_seperator"></div>`
          : ''
      }
      <div class="service_qa_unit" data-order="${order}">
      <!-- 服務問答: 標題 -->
      <div class="member-group">
        <input type="text" name="service_qa_question${order}" id="service_qa_question${order}"
          class="member-input service_qa_question service_qa_question${order}" placeholder="請輸入問題敘述
          ">

        <span class="error_span member_error_hint">此項目不得為空</span>
      </div>

      <!-- 服務問答: 敘述 -->
      <div class="member-group">
        <input type="text" name="service_qa_answer${order}" id="service_qa_answer${order}"
          class="member-input service_qa_answer service_qa_answer1" placeholder="請輸入問題回覆
          ">

        <span class="error_span member_error_hint">此項目不得為空</span>
      </div>

    </div>
    `;

    // 放入 Dom 裡
    $('#service_qa_body').append(html);
  }

  /**
   * @author odin
   * @description 分幾個部分進行檢查，全部通過就回傳 true 
   * @return {boolean}
   */
  function checkAllRight() {
    const isCheckboxPass = checkCheckboxes();
    const isOtherInputPass = checkOtherInputs();
    // const isAddableInputPass = checkAddableInput();

    return isCheckboxPass && isOtherInputPass;
  }

  /**
   * @author odin
   * @description 檢查所有的checkbox組是否都有勾選
   * @return {boolean}
   */
  function checkCheckboxes() {
    let isPass = true;
    const checkList = [
      '.checkbox_district',
      '.checkbox_type',
      '.checkbox_item',
      '.checkbox_detail',
      '.checkbox_age',
      '.checkbox_mode',
      '.checkbox_time',
      '.checkbox_week',
      '.checkbox_time_intercept',
      '.checkbox_place',
    ];

    checkList.forEach(function (item) {
      const totalLength = $(item).length;
      let count = 0;
      $(item).each(function (index, checkboxDom) {
        $(this).prop('checked') ? count++ : null;

        // 最後一個得時候判斷是否要加 error class
        if (totalLength - 1 === index) {
          if (count === 0) {
            $(this).closest('.service_form_unit').addClass('error');
            isPass = false;
            return;
          } else if (count > 0) {
            $(this).closest('.service_form_unit').removeClass('error');
          }
        }
      });
    });

    console.log('isPass', isPass);

    return isPass;
  }

  /**
   * @author odin
   * @description 檢查 服務報價 以及 服務相關影片的網址
   * @return {boolean}
   */
  function checkOtherInputs() {
    let isPass = true;

    // 檢查服務報價
    if (!isEmpty($('#service_price').val().trim())) {
      $('#service_price').closest('.service_form_unit').removeClass('error');
    } else {
      $('#service_price').closest('.service_form_unit').addClass('error');
      isPass = false;
    }

    // 檢查服務相關影片的網址是否有輸入
    if (!isEmpty($videoIframeTextarea.val().trim())) {
      $videoIframeTextarea.closest('.service_form_unit').removeClass('error');
    } else {
      $videoIframeTextarea.closest('.service_form_unit').addClass('error');
      isPass = false;
    }

    return isPass;
  }

  /**
   * @author odin
   * @description 檢查 服務案例 以及 服務問答 的Input是不是都有輸入
   * @return {boolean}
   */
  function checkAddableInput() {
    let isPass = true;

    // 檢查 服務案例
    serviceCaseUnitArray.forEach(function (item) {
      // 服務案例的 title
      if (!isEmpty(item.title)) {
        $('#service_case_title' + item.order)
          .closest('.member-group')
          .removeClass('error');
      } else {
        isPass = false;
        $('#service_case_title' + item.order)
          .closest('.member-group')
          .addClass('error');
      }

      // 服務案例的 description
      if (!isEmpty(item.description)) {
        $('#service_case_description' + item.order)
          .closest('.member-group')
          .removeClass('error');
      } else {
        isPass = false;
        $('#service_case_description' + item.order)
          .closest('.member-group')
          .addClass('error');
      }
    });

    // 檢查 服務問答
    serviceQAUnitArray.forEach(function (item) {
      // 服務問答的 question
      if (!isEmpty(item.question)) {
        $('#service_qa_question' + item.order)
          .closest('.member-group')
          .removeClass('error');
      } else {
        isPass = false;
        $('#service_qa_question' + item.order)
          .closest('.member-group')
          .addClass('error');
      }

      // 服務問答的 answer
      if (!isEmpty(item.description)) {
        $('#service_qa_answer' + item.order)
          .closest('.member-group')
          .removeClass('error');
      } else {
        isPass = false;
        $('#service_qa_answer' + item.order)
          .closest('.member-group')
          .addClass('error');
      }
    });

    return isPass;
  }

  // Variables =================================================

  // Dom
  // const $addNewServiceCaseBtn = $('#add_new_service_case_btn');
  // const $addNewServiceQABtn = $('#add_new_service_qa_btn');
  const $uploadServiceRelatedContainer = $('#upload_service_related_container');
  const $classificationBtns = $('.service_item_classification_btn');
  const $classificationShowSection = $('.classification_show_section');
  const $videoIframeTextarea = $('#service_related_video_iframe');

  const $tooltipTrigger = $('#service_related_video_tooltip');
  const $iframeNextStepBtn = $('#next-step-btn');
  const $iframePrevStepBtn = $('#prev-step-btn');
  const $step1Container = $('#js-step1');
  const $step2Container = $('#js-step2');

  // 產生 服務相關照片 的依據
  // let serviceRelatedUnitArray = [];
  // 產生新的 服務案例 的依據
  // let serviceCaseUnitArray = [];
  // 產生新的 服務問答 的依據
  // let serviceQAUnitArray = [];

  // Initialization =================================================
  // init();

  // Events =================================================

  // 服務相關照片 圖片上傳控制
  (function () {
    // 新增一組上傳的 group
    $('#add_service_related_upload_group_btn').bind('click', function (e) {
      e.preventDefault();

      generateServiceRelatedUnit();
    });

    // 選擇要上傳的圖片
    $mainRoot.on('click', '.service_related_upload_label', function () {
      const order = $(this).closest('.upload_certificate_group').data('order');

      $('#service_related_upload' + order).trigger('click');
    });

    // 修改畫面
    $mainRoot.on('change', '.service_related_upload', function () {
      const vm = this;
      const $targetImgDom = $(this).siblings('.member-uploaded-img');
      const $siblingsLabelDom = $(this).siblings(
        '.service_related_upload_label',
      );
      const order = $(this).closest('.upload_certificate_group').data('order');

      // 要給 function 指定特定的 Dom 元素操作
      readURL(vm, function (e) {
        console.log('readURL e', e);
        if (e.target.result) {
          // 顯示圖片
          $targetImgDom.attr('src', e.target.result).removeClass('hidden');
          $siblingsLabelDom.addClass('hidden');

          // 放置圖片的物件
          serviceRelatedUnitArray[order - 1].imgObj = e;

          console.log('serviceRelatedUnitArray', serviceRelatedUnitArray);
        }
      });
    });
  })();

  // 服務案例
  (function () {
    // 新增 服務案例 的內容
    // $addNewServiceCaseBtn.bind('click', function (e) {
    //   e.preventDefault();

    //   generateServiceCaseUnit();
    // });

    // 服務案例的圖片上傳(選擇點擊)
    $mainRoot.on('click', '.service_case_upload_label', function () {
      // const order = $(this).closest('.service_case_unit').data('order');

      // $('#service_case_img_upload' + order).trigger('click');

      $(this).siblings('.service_case_img_upload').trigger('click');
    });

    // 服務案例的圖片上傳(讀取圖片並且放入對應的資料中)
    $mainRoot.on('change', '.service_case_img_upload', function () {
      const vm = this;
      const $targetImgDom = $(this)
        .siblings('.member-uploaded-img-box')
        .find('.member-uploaded-img');
      const $siblingsLabelDom = $(this).siblings('.upload_certificate_label');
      const $siblingsDeleteDom = $(this)
        .siblings('.delete_btn_box')
        .removeClass('hidden');
      // let order = $(this).closest('.service_case_unit').data('order');

      // 要給 function 指定特定的 Dom 元素操作
      readURL(vm, function (e) {
        console.log('readURL e', e);
        if (e.target.result) {
          // 顯示圖片
          $targetImgDom.attr('src', e.target.result).removeClass('hidden');
          $siblingsDeleteDom.removeClass('hidden');
          $siblingsLabelDom.addClass('hidden');
          $siblingsLabelDom.addClass('hidden');

          // 放置圖片的物件
          // serviceCaseUnitArray[order - 1].imgObj = e;
        }
      });
    });

    // 服務案例: 標題 => 檢查並且把值送入資料中
    $mainRoot.on(
      'blur',
      '.service_case_title',
      debounce(function () {
        let order = $(this).closest('.service_case_unit').data('order');
        const value = $(this).val().trim();
        const $errorDom = $(this).closest('.member-group');

        if (!isEmpty(value)) {
          // 移除錯誤樣式
          $errorDom.removeClass('error');

          // 放置資料到正確的位置
          serviceCaseUnitArray[order - 1].title = value;
        } else {
          // 移除錯誤樣式
          $errorDom.addClass('error');

          // 放置資料到正確的位置
          serviceCaseUnitArray[order - 1].title = value;
        }
      }, 100),
    );

    // 服務案例: 敘述 => 檢查並且把值送入資料中
    $mainRoot.on(
      'blur',
      '.service_case_description',
      debounce(function () {
        let order = $(this).closest('.service_case_unit').data('order');
        const value = $(this).val().trim();
        const $errorDom = $(this).closest('.member-group');

        if (!isEmpty(value)) {
          // 移除錯誤樣式
          $errorDom.removeClass('error');

          // 放置資料到正確的位置
          serviceCaseUnitArray[order - 1].description = value;
        } else {
          // 移除錯誤樣式
          $errorDom.addClass('error');

          // 放置資料到正確的位置
          serviceCaseUnitArray[order - 1].description = value;
        }
      }, 100),
    );
  })();

  // 服務問答
  // (function () {
  //   // 新增 服務問答 的內容
  //   $addNewServiceQABtn.bind('click', function (e) {
  //     e.preventDefault();

  //     generateQAUnit();
  //   });

  //   // 服務問答: 問題敘述 => 檢查並且把值送入資料中
  //   $mainRoot.on(
  //     'blur',
  //     '.service_qa_question',
  //     debounce(function () {
  //       let order = $(this).closest('.service_qa_unit').data('order');
  //       const value = $(this).val().trim();
  //       const $errorDom = $(this).closest('.member-group');

  //       if (!isEmpty(value)) {
  //         // 移除錯誤樣式
  //         $errorDom.removeClass('error');

  //         // 放置資料到正確的位置
  //         serviceQAUnitArray[order - 1].question = value;
  //       } else {
  //         // 移除錯誤樣式
  //         $errorDom.addClass('error');

  //         // 放置資料到正確的位置
  //         serviceQAUnitArray[order - 1].question = value;
  //       }
  //     }, 100),
  //   );

  //   // 服務問答: 問題敘述 => 檢查並且把值送入資料中
  //   $mainRoot.on(
  //     'blur',
  //     '.service_qa_answer',
  //     debounce(function () {
  //       let order = $(this).closest('.service_qa_unit').data('order');
  //       const value = $(this).val().trim();
  //       const $errorDom = $(this).closest('.member-group');

  //       if (!isEmpty(value)) {
  //         // 移除錯誤樣式
  //         $errorDom.removeClass('error');

  //         // 放置資料到正確的位置
  //         serviceQAUnitArray[order - 1].answer = value;
  //       } else {
  //         // 移除錯誤樣式
  //         $errorDom.addClass('error');

  //         // 放置資料到正確的位置
  //         serviceQAUnitArray[order - 1].answer = value;
  //       }
  //     }, 100),
  //   );
  // })();

  // 點下 儲存 後
  $('#submit_service_form_btn').bind('click', function (e) {
    e.preventDefault();
    const allPass = checkAllRight();

    if (allPass) {
      // 成功的 API
    }
  });

  // 點下分類按鈕
  $classificationBtns.bind('click', function (e) {
    e.preventDefault();

    const target = $(this).data('target');

    // 按鈕樣式處理
    $classificationBtns.removeClass('active');
    $(this).addClass('active');

    // 內容顯示處裡
    $classificationShowSection.addClass('hidden');
    $(target).removeClass('hidden');
  });

  // 服務項目
  (function () {
    // 服務項目 開啟燈箱
    $tooltipTrigger.bind('click', function (e) {
      e.preventDefault();
      showLightbox('#lightbox-iframe-step');
    });

    // 服務項目的燈箱內容切換
    $iframeNextStepBtn.bind('click', function () {
      console.log('next');
      $step1Container.addClass('hidden');
      $step2Container.removeClass('hidden');
    });

    $iframePrevStepBtn.bind('click', function () {
      $step2Container.addClass('hidden');
      $step1Container.removeClass('hidden');
    });
  })();
});
