$(function () {
  // Variables ===============================

  // Doms
  const $memberInput = $('.member-input');

  // Events =============================
  if ($memberInput.length > 0) {
    // 輸入檢查
    $memberInput.bind(
      'keyup',
      debounce(function () {
        const value = $(this).val();
        const type = $(this).attr('type');

        // error class toggle
        if (type === 'email') {
          if (!isEmpty(value) && isEmailCorrect(value)) {
            $(this).closest('.member-group').removeClass('error');
          } else {
            $(this).closest('.member-group').addClass('error');
          }
        } else {
          if (value.trim() !== '') {
            $(this).closest('.member-group').removeClass('error');
          } else {
            $(this).closest('.member-group').addClass('error');
          }
        }
      }, 500),
    );
  }
});
