$(function () {
  // Functions ===========================

  /**
   * @author odin
   * @param {string} $DomId -- 要關閉的 Modal Id: eg. #login_modal
   * @param {string} statusName -- 要關閉的 Modal Starus Name，可以參照 modalStatus 內的屬性
   * @param {boolean} isCloseMaskAndContainer -- 是否要連同 container 以及 mask 一併隱藏，預設為 false
   * @description 關閉特定的 Modal
   */
  function closeModal($DomId, statusName, isCloseMaskAndContainer) {
    // 預設
    isCloseMaskAndContainer =
      typeof isCloseMaskAndContainer === 'boolean'
        ? isCloseMaskAndContainer
        : false;

    // 設定 class
    $($DomId).removeClass('animate__fadeInDown').addClass('animate__fadeOutUp');

    // 設定狀態
    modalStatus[statusName] = false;

    if (isCloseMaskAndContainer) {
      // mask 消失
      $modalMask.fadeOut(500).removeAttr('style');

      // 移除 overflow-hidden
      $('body').removeClass('overflow-hidden');

      setTimeout(function () {
        // container 消失
        $containers.removeAttr('style');
      }, 500);
    }
  }

  /**
   * @author odin
   * @param {string} $DomId -- 要關閉的 Modal Id: eg. #login_modal
   * @param {string} statusName -- 要關閉的 Modal Starus Name，可以參照 modalStatus 內的屬性
   * @param {boolean} isOpenMaskAndContainer -- 是否要連同 container 以及 mask 一併開啟，預設為 true
   * @description 關閉特定的 Modal
   */
  function openModal($DomId, statusName, isOpenMaskAndContainer) {
    // 預設
    isOpenMaskAndContainer =
      typeof isOpenMaskAndContainer === 'boolean'
        ? isOpenMaskAndContainer
        : true;

    // 清除所有的錯誤樣式
    clearError();

    // 清除所有的 Input 內容
    clearInput();

    // 移除其他的燈箱被開啟的可能性
    $('.animate__fadeOutUp')
      .removeAttr('style')
      .removeClass('animate__fadeOutUp');

    // 設定 class
    $($DomId).show().addClass('animate__fadeInDown');

    // 設定狀態
    modalStatus[statusName] = true;

    if (isOpenMaskAndContainer) {
      // mask 開啟
      $modalMask.fadeIn();

      // overflow-hidden
      $('body').addClass('overflow-hidden');

      // container 開啟
      $containers.show();
    }
  }

  /**
   * @author odin
   * @description 清除所有的錯誤樣式
   */
  function clearError() {
    $('.error').removeClass('error');
  }

  /**
   * @author odin
   * @description 清除所有的 Input 的內容
   */
  function clearInput() {
    $modalInput.val('');
  }

  // Variables ===========================

  let modalStatus = {
    login: false,
    signup: false,
    forgetpwd: false,
    sent: false,
    resetpwd: false,
  };

  const $resentEmail = $('#resent_email');

  // Modal 開關控制
  const $containers = $('#modal-container');
  const $modalCloseBtn = $('.modal-close');
  const $modalMask = $('#modal-mask');
  const $modalCtrl = $('.modal-ctrl');

  // 送出表單
  const $loginSubmit = $('#login_submit');
  const $modalInput = $('.modal-input');
  const $signupSubmit = $('#signup_submit');
  const $forgetPwdSubmit = $('#forget_pwd_submit');
  const $resetPwdSubmit = $('#reset_pwd_submit');

  // Initialization ===========================

  // 打開重設密碼的js
  // openModal('#reset_pwd_modal', 'resetpwd', true);

  // Events ===========================

  // 關閉 Modal
  $modalCloseBtn.bind('click', function () {
    const $target = $(this).data('target');
    const statusName = $(this).data('status');

    closeModal($target, statusName, true);
  });

  // 開啟 Modal
  $modalCtrl.bind('click', function () {
    const $openTarget = $(this).data('target');
    const openStatusName = $(this).data('status');
    const $closeTarget = $(this).data('close-target');
    const closeStatusName = $(this).data('close-status');
    const thisId = $(this).attr('id');
    let closeBoolean = null;

    closeBoolean = thisId === 'sent_sure' ? true : false;

    if ($closeTarget && closeStatusName) {
      closeModal($closeTarget, closeStatusName, closeBoolean);
    }

    if ($openTarget && openStatusName) {
      openModal($openTarget, openStatusName);
    }
  });

  // 表單送出 =================================================================

  // 輸入檢查
  $modalInput.bind(
    'keyup',
    debounce(function () {
      const errorDom = $(this).siblings('.error_hint');
      const addErrorClassDom = $(this).parent();
      const value = $(this).val().trim();
      const type = $(this).data('type');

      // error class toggle
      if (type === 'email') {
        if (!isEmpty(value) && isEmailCorrect(value)) {
          addErrorClassDom.removeClass('error');
        } else {
          addErrorClassDom.addClass('error');
        }
      } else if (type === 'pwd') {
        if (!isEmpty(value) && value.length >= 6 && value.length <= 8) {
          addErrorClassDom.removeClass('error');
        } else {
          addErrorClassDom.addClass('error');
        }
      } else if (type === 'pwd_confirm') {
        const confirmTarget = $(this).data('confirm-target');

        if (!isEmpty(value) && value === $(confirmTarget).val()) {
          addErrorClassDom.removeClass('error');
        } else {
          addErrorClassDom.addClass('error');
        }
      } else {
        if (!isEmpty(value)) {
          addErrorClassDom.removeClass('error');
        } else {
          addErrorClassDom.addClass('error');
        }
      }
    }, 500),
  );

  // 登入表單送出檢查
  $loginSubmit.bind('click', function () {
    let allPass = true;
    const email = $('#login_email').val();
    const password = $('#login_pwd').val();

    // Email 檢查
    if (isEmpty(email) || !isEmailCorrect(email)) {
      $('#login_email').parent().addClass('error');
      allPass = false;
    }

    // 密碼檢查
    if (isEmpty(password)) {
      $('#login_pwd').parent().addClass('error');
      allPass = false;
    }

    if (allPass) {
      // 做登入的動作
    }
  });

  // 註冊表單送出檢查
  $signupSubmit.bind('click', function () {
    let allPass = true;
    const name = $('#signup_name').val();
    const email = $('#signup_email').val();
    const password = $('#signup_pwd').val();
    const confirmPwd = $('#signup_confirm_pwd').val();

    // 密碼檢查
    if (isEmpty(name)) {
      $('#signup_name').parent().addClass('error');
      allPass = false;
    }

    // Email 檢查
    if (isEmpty(email) || !isEmailCorrect(email)) {
      $('#signup_email').parent().addClass('error');
      allPass = false;
    }

    // 密碼檢查
    if (isEmpty(password) || password.length < 6 || password.length > 8) {
      $('#signup_pwd').parent().addClass('error');
      allPass = false;
    }

    // 密碼確認檢查
    if (isEmpty(confirmPwd) || confirmPwd !== password) {
      $('#signup_confirm_pwd').parent().addClass('error');
      allPass = false;
    }

    if (allPass) {
      // 註冊的動作
    }
  });

  // 忘記密碼 -- email的檢查
  $forgetPwdSubmit.bind('click', function () {
    let allPass = true;
    const email = $('#forget_pwd_email').val();

    // Email 檢查
    if (isEmpty(email) || !isEmailCorrect(email)) {
      $('#forget_pwd_email').parent().addClass('error');
      allPass = false;
    }

    if (allPass) {
      // 寄送認證信到指定的 Email

      // 關閉原本的燈箱
      closeModal('#forget_pwd_modal', 'forgetpwd');
      // 開啟已經寄送的 Modal
      openModal('#sent_modal', 'sent');
    }
  });

  // 重寄認證信
  $resentEmail.bind('click', function () {
    // 執行重寄認證信
  });

  // 重設密碼
  $resetPwdSubmit.bind('click', function () {
    let allPass = true;
    const resetNewPwd = $('#reset_new_pwd').val();
    const resetNewPwdConfirm = $('#reset_new_pwd_confirm').val();

    // 密碼檢查
    if (
      isEmpty(resetNewPwd) ||
      resetNewPwdConfirm.length < 6 ||
      resetNewPwdConfirm.length > 8
    ) {
      $('#reset_new_pwd').parent().addClass('error');
      allPass = false;
    }

    // 密碼確認檢查
    if (isEmpty(resetNewPwdConfirm) || resetNewPwdConfirm !== resetNewPwd) {
      $('#reset_new_pwd_confirm').parent().addClass('error');
      allPass = false;
    }

    if (allPass) {
      // 送出重設密碼的請求
    }
  });
});
