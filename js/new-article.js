$(function () {
  // Functions ===========================
  /**
   * @author odin
   * @description 確認是否都有填妥
   */
  function checkArticleInput() {
    const title = $('#title_input').val().trim();
    const articleShortDescription = $('#title_input').val().trim();
    const articleDescription = $('#article_description_textarea').val().trim();

    // 檢查文章標題是否有填
    if (isEmpty(title)) {
      $('#title_input').parent().addClass('error');
    } else {
      $('#title_input').parent().removeClass('error');
    }

    // 檢查文章概述是否有填寫
    if (isEmpty(articleShortDescription)) {
      $('#article_short_input').parent().addClass('error');
    } else {
      $('#article_short_input').parent().removeClass('error');
    }

    // 檢查文章描述述是否有填寫
    if (isEmpty(articleDescription)) {
      $('#article_description_textarea').parent().addClass('error');
    } else {
      $('#article_description_textarea').parent().removeClass('error');
    }

    // 檢查是否有勾選文章類別
    (function () {
      let categoryCount = 0;

      $categoryCheckbox.each(function () {
        console.log($(this).prop('checked'));
        if ($(this).prop('checked') === false) {
          categoryCount++;
        }
      });

      if (categoryCount === 6) {
        $('#category_checkbox_form_group').addClass('error');
      } else {
        $('#category_checkbox_form_group').removeClass('error');
      }
    })();

    // 檢查是否有上傳圖片
    if (reader === null) {
      $('#upload_img_group').addClass('error');
    } else {
      $('#upload_img_group').removeClass('error');
    }
  }

  // Variables ===========================

  // Image Reader
  let reader = null;

  // Doms
  const $uploadImageBtn = $('#upload_img_btn');
  const $uploadImageInput = $('#upload_img_input');
  const $saveArticleBtn = $('#save_article_btn');
  const saveAndPublishArticleBtn = $('#save_publish_article_btn');
  const $textInput = $('.text-input');
  const $textArea = $('.text-textarea');
  const $categoryCheckbox = $('.category_checkbox');

  // Events ===========================
  // 圖片上傳的操作
  (function () {
    // 點選編輯的按鈕
    $uploadImageBtn.bind('click', function () {
      $uploadImageInput.trigger('click');
    });

    // 圖片上傳之後
    $uploadImageInput.bind('change', function () {
      // 要給 function 指定特定的 Dom 元素操作
      readURL(this, function (e) {
        $('#preview_uploaded_hint').hide();

        $('#preview_uploaded_img')
          .attr('src', e.target.result)
          .removeClass('hidden')
          .parent()
          .addClass('had_img');

        $('#upload_img_group').removeClass('error');
      });
    });
  })();

  // 儲存文章
  $saveArticleBtn.bind('click', function (e) {
    e.preventDefault();

    // 檢查內容
    checkArticleInput();
  });

  // 儲存文章並發布
  saveAndPublishArticleBtn.bind('click', function (e) {
    e.preventDefault();

    // 檢查內容
    checkArticleInput();
  });

  // 使用者輸入或是針對特定的 input 操作後進行檢查
  (function () {
    // 輸入 Input 檢查
    $textInput.bind(
      'keyup',
      debounce(function () {
        const value = $(this).val();

        // error class toggle
        if (value.trim() !== '') {
          $(this).closest('.form-group').removeClass('error');
        } else {
          $(this).closest('.form-group').addClass('error');
        }
      }, 500),
    );

    // 輸入 textarea 檢查
    $textArea.bind(
      'keyup',
      debounce(function () {
        const value = $(this).val();

        // error class toggle
        if (value.trim() !== '') {
          $(this).closest('.form-group').removeClass('error');
        } else {
          $(this).closest('.form-group').addClass('error');
        }
      }, 500),
    );

    // 類別選擇的檢查
    $categoryCheckbox.bind('click', function () {
      let categoryCount = 0;

      $categoryCheckbox.each(function () {
        console.log($(this).prop('checked'));
        if ($(this).prop('checked') === false) {
          categoryCount++;
        }
      });

      if (categoryCount === 6) {
        $('#category_checkbox_form_group').addClass('error');
      } else {
        $('#category_checkbox_form_group').removeClass('error');
      }
    });
  })();
});
