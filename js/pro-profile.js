$(function () {
  // Functions ===========================
  /**
   * @author odin
   * @description Carousel 初始化
   */
  function initCarousel() {
    const allCarouselBody = $('.pro_carousel');

    // 註冊每一個 Carousel
    allCarouselBody.each(function () {
      const thisTargetId = $(this).attr('id');
      console.log('thisTargetId', thisTargetId);
      $('#' + thisTargetId).slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              infinite: true,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 640,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
    });
  }

  // Variables ===========================

  // Doms
  const allCategoryBtn = $('.pro_category_btn');
  const allCategoryContent = $('.category_content');

  // Initialization ===========================
  initCarousel();

  // Events ===========================
  // 點選類別
  allCategoryBtn.bind('click', function () {
    const targetCategoryDom = $(this).data('target');
    const hasCarousel = +$(this).data('carousel') ? true : false;

    allCategoryBtn.removeClass('active');
    $(this).addClass('active');

    allCategoryContent.addClass('hidden');
    $(targetCategoryDom).removeClass('hidden');

    if (hasCarousel) {
      $('.slick-next').trigger('click');
      $('.slick-prev').trigger('click');
    }
  });
});
