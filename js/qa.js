$(function () {
  // Functions ===========================

  function scrollParentToChild(parent, child) {
    // Where is the parent on page
    var parentRect = parent.getBoundingClientRect();
    // What can you see?
    var parentViewableArea = {
      height: parent.clientHeight,
      width: parent.clientWidth,
    };

    // Where is the child
    var childRect = child.getBoundingClientRect();
    // Is the child viewable?
    // var isViewable =
    //   childRect.top >= parentRect.top &&
    //   childRect.top <= parentRect.top + parentViewableArea.height;

    // if you can't see the child try to scroll parent
    // if (!isViewable) {
    // scroll by offset relative to parent
    parent.scrollTop = childRect.top + parent.scrollTop - parentRect.top;
    // }
  }

  // Variables ===========================

  // Doms
  const $qaCategoryBtn = $('.qa_category_btn');
  const $qaContentBox = $('.qa_content_box');
  const $jsQaUnit = $('.js_qa_unit');
  const $jsQaTitle = $('.js_qa_title');
  const $jsQaAnswer = $('.js_qa_answer');

  // Events ===========================

  // 切換類別
  $qaCategoryBtn.bind('click', function () {
    const target = $(this).data('target');

    $qaCategoryBtn.removeClass('active');
    $(this).addClass('active');

    $qaContentBox.addClass('hidden');
    $(target).removeClass('hidden');

    // 收合所有的答案
  });

  // 收合回答
  $jsQaTitle.bind('click', function () {
    const $thisAnswer = $(this).siblings('.js_qa_answer');
    const $thisQAUnit = $(this).closest('.qa_unit');
    const $thisContent = $(this).closest('.qa_content_box');
    const isOpen = $thisQAUnit.hasClass('open');

    const thisId = $thisQAUnit.attr('id');
    const $child = document.getElementById(thisId);
    // const $parent = document.getElementById('qa_content');
    const $parent = $child.parentNode;

    if (isOpen) {
      $thisAnswer.slideUp();
      $thisQAUnit.removeClass('open');
    } else {
      $jsQaAnswer.slideUp();
      $thisAnswer.slideDown();

      $jsQaUnit.removeClass('open');
      $thisQAUnit.addClass('open');

      // var target = document.getElementById('target');
      // $scrollDom.parentNode.scrollTop = $scrollDom.offsetTop;

      // $thisQAUnit[0].scrollIntoView({
      //   behavior: 'smooth',
      //   block: 'nearest',
      //   inline: 'start',
      // });

      // $thisQAUnit[0].scrollIntoView({ behavior: 'smooth' });

      // scrollParentToChild($parent, $child);

      // scrollToThisElement();
      // var offset = $thisQAUnit.offset();
      // offset.left -= 20;
      // offset.top -= 20;

      // $thisContent.animate({
      //   scrollTop: offset.top,
      //   scrollLeft: offset.left,
      // });
    }
  });
});
